<?php get_header(); ?>
<div class="container" style="min-height: 69vh;">
  <div class="row mt-4">
    <div class="col">
      <?php while ( have_posts() ) : the_post();?>
        <div class="text-center">
          <h1><?php the_title() ?></h1>
          <?php the_post_thumbnail( 'medium_large', ['class' => 'img-fluid'] ) ?>
        </div>

        <div class="page-header mt-3">
          <small>Publicado: <?php the_time('F jS, Y') ?></small>
        </div>

        <div class="mt-4">
          <?php the_content('Read the rest of this entry &raquo;'); ?>
        </div>
      <?php endwhile; ?>
      </div>
      <?php if ( is_active_sidebar( 'primary' ) AND is_singular('job_listing' ) ): ?>
        <?php get_sidebar( 'primary' ); ?>
      <?php endif ?>
    </div>
  </div>
</div>
<?php get_footer(); ?>