<?php
require_once dirname( __FILE__ ) . '/helpers/TGM-Plugin-Activation-2.6.1/class-tgm-plugin-activation.php';
require_once dirname( __FILE__ ) . '/helpers/class-wp-bootstrap-navwalker.php';

require_once dirname( __FILE__ ) . '/helpers/required-plugins.php';
require_once dirname( __FILE__ ) . '/helpers/theme-settings.php';
// require_once dirname( __FILE__ ) . '/helpers/rest_custom_endpoints.php';

// require_once dirname( __FILE__ ) . '/helpers/slider-cpt.php';
// require_once dirname( __FILE__ ) . '/helpers/slider-metabox.php';

add_theme_support( 'post-thumbnails' );

function emamut_setup() {
  load_theme_textdomain( 'emamut' );
}
add_action( 'after_setup_theme', 'emamut_setup' );

function add_theme_scripts() {
  wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', array(), '1.1', 'all');
  wp_enqueue_style('fontawesome', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css', array(), '1.1', 'all');

  wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.css');

  wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.3.1.min.js', array (), 1.1, true);
  wp_enqueue_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array (), 1.1, true);
  wp_enqueue_script('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', array (), 1.1, true);

  wp_enqueue_script('helpers.js', get_template_directory_uri() . '/assets/js/helpers.js', array (), 1.1, true);
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

function register_menus() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_menus' );

add_action( 'widgets_init', 'my_register_sidebars' );
function my_register_sidebars() {
  /* Register the 'primary' sidebar. */
  register_sidebar(
    array(
      'id'            => 'primary',
      'name'          => __( 'Primary Sidebar' ),
      'description'   => __( 'A short description of the sidebar.' ),
      'before_widget' => '<div class="col mb-4"><div id="%1$s" class="card sidebar %2$s">',
      'after_widget'  => '</div></div>',
      'before_title'  => '<div class="card-header">',
      'after_title'   => '</div>',
    )
  );
  /* Repeat register_sidebar() code for additional sidebars. */
}