jQuery(document).ready(function ($) {
  $(window).on('scroll', function () {
    if ($(this).scrollTop() >= 50)
      $('#logo').width('50%')
    else
      $('#logo').width('100%')
  })

  $.ajaxSetup({ cache: true })
  $.getScript('https://connect.facebook.net/en_US/sdk.js', function(){
    FB.init({
      appId: '882788505402180',
      version: 'v3.3'
    })
    FB.api('974223192590980/videos?fields=embed_html,picture,title', { access_token: 'EAAMi5DdGu0QBACv0BqQ8vZC8DDfEwH3gCKVZCO4RN4Q3pvMrJHZA88PgPJlBuf1URHhNSeodIekSSiWcP1jMDI86BVq3IylhJQGlnBGQHGdZCNPaO4ngTmxqZA87zibmFCJBVsctXjRnkaQMEIYWKfdsnRtFugrURpXigYZBNOdwZDZD' }, function (response) {
      var videos = response.data.slice(0, 8)
      videos.forEach(function(video) {
        var title = typeof video.title === 'undefined' ? '' : video.title
        $('#videos-row').append('<div class="col-md-3 col-xs-12">\
        <div class="card" data-id="' + video.id + '" data-title="' + title + '" data-toggle="modal" data-target="#video-modal">\
          <img src="' + video.picture + '" alt="" class="card-img-top">\
          <div class="card-body">\
            <p class="card-title">' + title + '</p>\
          </div>\
        </div></div>')
      })
    })
  })

  $(document).on('click', '#videos-row .card', function (e) {
    $('#video-modal').modal('toggle')
  })

  $('#video-modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget)

    var modal = $(this)
    modal.find('.modal-title').html(button.data('title'))
    modal.find('.modal-body iframe').attr('src', 'https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2FElCantonalCevallos%2Fvideos%2F' + button.data('id') + '%2F&width=720')
  })

  $('#video-modal').on('hidden.bs.modal', function (e) {
    var modal = $(this)
    modal.find('.modal-body iframe').attr('src', '')
  })
})