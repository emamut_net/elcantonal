<?php get_header() ?>

<div class="container">
  <div class="row mt-4">
    <div class="col">
      <h1 class="search-title text-center">
        <?php echo $wp_query->found_posts; ?> <?php _e( 'Resultados Encontrados Para', 'locale' ); ?>: "<?php the_search_query(); ?>"
      </h1>
    </div>
  </div>
  <?php if ( have_posts() ) : ?>
  <div class="card-deck">
    <?php while ( have_posts() ) : the_post(); ?>
    <div class="col-12 col-md-4">
      <div class="card mt-4">
        <?php  the_post_thumbnail('medium', array('class' => 'card-img-top')) ?>
        <h5 class="card-title"><a href="<?php echo get_permalink(); ?>"><?php the_title() ?></a></h5>
        <p class="card-text"><?php echo substr(get_the_excerpt(), 0,200); ?> <a href="<?php the_permalink(); ?>">Leer más</a></p>
      </div>
    </div>
    <?php endwhile ?>
  </div>
  <?php endif ?>
</div>

<?php get_footer() ?>