<?php get_header(); ?>
<div class="container" style="min-height: 69vh;">
  <div class="row mt-4">
    <div class="col">
      <h1 class="text-center"><?php echo single_cat_title( '', false ); ?></h1>
      <ul class="list-unstyled" id="category-list">
      <?php while ( have_posts() ) : the_post();?>
          <li class="media mt-3">
            <?php the_post_thumbnail( 'thumbnail', ['class' => 'img-fluid mr-3'] ) ?>
            <div class="media-body">
              <a href="<?php the_permalink() ?>"><h5><?php the_title() ?></h5></a>
              <p><?php the_excerpt() ?></p>
            </div>
          </li>
      <?php endwhile; ?>
      </ul>
    </div>
    <?php if ( is_active_sidebar( 'primary' ) AND is_singular('job_listing' ) ): ?>
      <?php get_sidebar( 'primary' ); ?>
    <?php endif ?>
  </div>
</div>
<?php get_footer(); ?>