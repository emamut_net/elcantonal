<?php get_header();

$args = array(
  'posts_per_page' => 5,
  'order_by' => 'date',
  'order' => 'DESC',
	'post__in' => get_option( 'sticky_posts' ),
	'ignore_sticky_posts' => 1
);
$sticky = new WP_Query($args);

$args = array(
  'posts_per_page' => 5,
  'order_by' => 'date',
  'order' => 'DESC',
  'category_name' => 'deportes'
);
$deportes = new WP_Query($args);

$args = array(
  'posts_per_page' => 5,
  'order_by' => 'date',
  'order' => 'DESC',
  'category_name' => 'cevallos'
);
$cevallos = new WP_Query($args);

$video_iframe = get_option('video_iframe');
if(!empty($video_iframe))
  $video_iframe = str_replace( '<iframe ', '<iframe class="embed-responsive-item" ', $video_iframe );

if(!empty($video_iframe))?>
  <div class="container" style="min-height: 69vh;">
    <div class="row mt-4">
      <div class="col">
        <?php if(empty($video_iframe)): ?>
        <div id="main-carousel" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <?php while($sticky->have_posts()): $sticky->the_post() ?>
            <div class="carousel-item <?php if($sticky->current_post == 0) echo 'active' ?>">
              <img src="<?php the_post_thumbnail_url() ?>" class="d-block w-100" alt="...">
              <div class="carousel-caption text-left">
                <a href="<?php echo get_permalink() ?>"><h5><?php echo $post->post_title ?></h5></a>
                <p>Publicado por: <?php the_author() ?> | <?php the_date() ?></p>
              </div>
            </div>
            <?php endwhile ?>
          </div>
          <a class="carousel-control-prev" href="#main-carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#main-carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        <?php else: ?>
        <div class="embed-responsive embed-responsive-16by9">
          <?php echo $video_iframe ?>
        </div>
        <?php endif ?>
        <div class="row mt-4">
          <div class="col-md-6 col-xs-12">
            <div class="card section national">
              <h5 class="card-header">Cevallos</h5>
              <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $cevallos->posts[0]->ID ), 'single-post-thumbnail' ); ?>
              <img src="<?php echo $image[0]; ?>" alt="" class="card-img-top">
              <div class="card-body">
                <h6 class="card-title"><a href="<?php echo get_post_permalink($cevallos->posts[0]->ID) ?>">
                  <?php echo $cevallos->posts[0]->post_title ?><br>
                    <small><?php echo get_the_date($cevallos->posts[0]->post_id) ?></small>
                  </h6>
                </a>
              </div>
              <ul class="list-group list-group-flush">
                <?php foreach ($cevallos->posts as $key => $post):
                  if($key > 0): ?>
                    <a href="<?php echo get_permalink() ?>" class="list-group-item list-group-item-action">
                      <div class="d-flex flex-row">
                        <img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ) )[0] ?>" alt="" class="w-25 h-25">
                        <p class="ml-2"><?php echo $post->post_title ?></p>
                      </div>
                    </a>
                  <?php endif;
                endforeach ?>
              </ul>
            </div>
          </div>
          <div class="col-md-6 col-xs-12">
            <div class="card section sports">
              <h5 class="card-header">Deportes</h5>
              <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $deportes->posts[0]->ID ), 'single-post-thumbnail' ); ?>
              <img src="<?php echo $image[0]; ?>" alt="" class="card-img-top">
              <div class="card-body">
                <h6 class="card-title"><a href="<?php echo get_post_permalink($deportes->posts[0]->ID) ?>">
                  <?php echo $deportes->posts[0]->post_title ?><br>
                    <small><?php echo get_the_date($deportes->posts[0]->post_id) ?></small>
                </a>
                </h6>
              </div>
              <ul class="list-group list-group-flush">
                <?php foreach ($deportes->posts as $key => $post):
                  if($key > 0): ?>
                    <a href="<?php echo get_permalink() ?>" class="list-group-item list-group-item-action">
                      <div class="d-flex flex-row">
                        <img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id ( $post->ID ) )[0] ?>" alt="" class="w-25 h-25">
                        <p class="ml-2"><?php echo $post->post_title ?></p>
                      </div>
                    </a>
                  <?php endif;
                endforeach ?>
              </ul>
            </div>
          </div>
        </div>
        <div class="row my-4">
          <div class="col">
            <div class="card section videos">
              <h5 class="card-header">Vídeos</h5>
              <div class="card-body">
                <div class="row" id="videos-row"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-xs-12">
        <?php get_sidebar('primary'); ?>
      </div>
    </div>
  </div>
<?php get_footer() ?>