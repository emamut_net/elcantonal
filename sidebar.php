<?php
$args = array(
  'posts_per_page' => 5,
  'order_by' => 'date',
  'order' => 'DESC'
);

$latest = new WP_Query($args) ?>

<!-- <div class="row">
  <div class="col">
    <input type="text" class="form-control" placeholder="Buscar...">
  </div>
</div> -->
<div class="row">
  <div class="col">
    <div class="card sidebar">
      <div class="card-header">
        SIGUENOS
      </div>
      <ul class="list-group list-group-flush">
        <a href="https://www.facebook.com/ElCantonalCevallos" target="_blank" class="list-group-item follow">
          <span class="fa-stack fa-2x facebook">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
          </span>
          ElCantonalCevallos
        </a>
        <a href="#" target="_blank" class="list-group-item follow">
          <span class="fa-stack fa-2x whatsapp">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fab fa-whatsapp fa-stack-1x fa-inverse"></i>
          </span>
          +593 98 64 900 53
        </a>
      </ul>
    </div>
  </div>
</div>
<div class="row">
  <div class="col mt-4">
    <div class="card sidebar">
      <div class="card-header">
        NUESTRA REVISTA
      </div>
      <div class="card-body p-0">
        <iframe style="border:none;width:100%;height:315px;" src="//e.issuu.com/embed.html#38151403/70017640" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>
<div class="row d-none d-md-block d-lg-block">
  <div class="col mt-4">
    <div class="card sidebar">
      <div class="card-header">
        ÚLTIMAS NOTICIAS
      </div>
      <ul class="list-group list-group-flush">
        <?php foreach ($latest->posts as $post): ?>
          <a href="<?php echo get_post_permalink($post->ID) ?>" class="list-group-item"><?php echo $post->post_title ?></a>
        <?php endforeach ?>
      </ul>
    </div>
  </div>
</div>