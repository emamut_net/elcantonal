<?php get_header(); ?>

  <div class="container">
    <div class="row mt-4">
      <div class="col">
        <?php
          while ( have_posts() ) : the_post(); ?>
            <div class="entry-content-page">
              <?php the_content(); ?>
            </div>
          <?php endwhile;
          wp_reset_query(); ?>
      </div>
      <div class="col-md-3 col-xs-12">
        <?php get_sidebar(); ?>
      </div>
    </div>
  </div>

<?php get_footer() ?>
