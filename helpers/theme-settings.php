<?php
function theme_option_page()
{ ?>
  <div class="wrap">
    <h1>Personalización del tema</h1>
    <form method="post" action="options.php">
      <?php settings_fields("theme-options-grp");
      do_settings_sections("theme-options");
      submit_button(); ?>
    </form>
  </div>
<?php }

function add_theme_menu_item()
{
  add_theme_page("Personalización del tema", "Personalización del tema", "manage_options", "theme-options", "theme_option_page", null, 99);
}
add_action("admin_menu", "add_theme_menu_item");


function display_video_iframe()
{ ?>
  <textarea name="video_iframe" id="video_iframe" cols="50" rows="3"><?php echo get_option('video_iframe'); ?></textarea>
<?php }

function theme_settings()
{
  add_settings_section( 'first_section', 'Video en vivo', '', 'theme-options');

  add_settings_field('video_iframe', 'Código del iFrame', 'display_video_iframe', 'theme-options', 'first_section');
  register_setting( 'theme-options-grp', 'video_iframe');
}
add_action('admin_init', 'theme_settings');

add_action('admin_enqueue_scripts', 'my_admin_scripts');

function my_admin_scripts()
{
  wp_enqueue_media();
  wp_register_script('my-admin-js', get_template_directory_uri() . '/helpers/my-admin.js', array('jquery'));
  wp_enqueue_script('my-admin-js');
}
